

import '../lib/alm.dart';

void main() {
  print(Alm.version);
  // //...
  // ///
  print({}.isMap); //true
  print([].isMap); //false
  // //
  print(null.isMap); //false
  // //
  var map = {'alm': 2, 'foo': 3};
  print('# check keys');
  print(map is Map); //true
  print(map.has('alm')); //true
  print(map.has(['alm', 'foo'])); //true
  print(map.has(['alm','food'])); //false
  //
  // # check key val
  print(map.has('alm', 2)); //true
  print(map.has('foo', 3.1)); //false
  // //
  // // //...
  //
  // '# check timers'.printAsTitle();
  //
  print(Duration().between('08:00:00'.toDuration(),'18:00:00'.toDuration()));
}
